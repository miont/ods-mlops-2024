# Ods Mlops 2024

## Branch naming convention:
* `main` for releases
* `develop` for development
* `feat/<name>` for feature implementation, merge to `develop`

## Build & run
### Locally using PDM
    pdm venv create
    pdm install
    .venv/bin/python src/ods_mlops/example.py
### Using Docker
    docker build . -t ods-mlops
    docker run -it --rm --name ods-mlops ods-mlops
