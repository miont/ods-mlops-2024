class Pipeline:
    """Basic pipeline for ML"""

    def __init__(self) -> None:
        pass

    def run(self) -> None:
        """Run whole pipeline"""
        ...
