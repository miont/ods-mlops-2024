import os
import sys
from typing import Optional

import pandas as pd

_ = pd.DataFrame
print(sys.path)
_ = os.abort


def test(a: int, b: str, c: float, d: Optional[pd.DataFrame]) -> None:
    """Test function

    Args:
        a (int): parameter 1
        b (str): parameter 2
        c (float): parameter 3
        d (Optional[pd.DataFrame]): parameter 4
    """
    ...


def test2(a: str, b: bool) -> str:
    """Another test function

    Args:
        a (str): parameter 1
        b (bool): parameter 2

    Returns:
        str: result
    """
    return a if b else "Hello!"


test(1, "s", 2.1, None)
