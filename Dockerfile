FROM python:3.10-slim

WORKDIR /app

RUN apt-get update && apt-get install -y curl
RUN curl -sSL https://pdm-project.org/install-pdm.py | python3 -
ENV PATH="/root/.local/bin:${PATH}"
RUN pdm venv create
COPY pyproject.toml pdm.lock ./
RUN pdm install

COPY src/ods_mlops ./

ENTRYPOINT [ ".venv/bin/python", "example.py" ]
